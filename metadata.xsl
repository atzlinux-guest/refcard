<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:fo="http://www.w3.org/1999/XSL/Format"
		version="1.0">

  <!-- (C) 2008 W. Martin Borgert debacle@debian.org
       See COPYING for the license status of this software. -->

  <xsl:output method="text" encoding="utf-8"/>

  <xsl:template match="article/title">
    <xsl:text>InfoBegin
InfoKey: Title
InfoValue: </xsl:text><xsl:value-of select="."/><xsl:text>
</xsl:text>
  </xsl:template>

  <xsl:template match="article/subtitle">
    <xsl:text>InfoBegin
InfoKey: Subject
InfoValue: </xsl:text><xsl:value-of select="."/><xsl:text>
</xsl:text>
  </xsl:template>

  <xsl:template match="keyword[@role]"/>

  <xsl:template match="keyword">
    <xsl:if test="position() != 1">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="keywordset">
    <xsl:text>InfoBegin
InfoKey: Keywords
InfoValue: </xsl:text>
    <xsl:apply-templates select="*"/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  <xsl:template match="articleinfo">
    <xsl:text>InfoBegin
InfoKey: Author
InfoValue: </xsl:text><xsl:apply-templates select="copyright"/>
    <xsl:for-each select="copyright/holder">
      <xsl:value-of select="normalize-space(.)"/>
      <xsl:if test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="position() = last()">
	<xsl:text>
</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:apply-templates select="keywordset"/>
    <xsl:text>InfoBegin
InfoKey: Creator
InfoValue: docbook-xsl, xsltproc
InfoBegin
InfoKey: Producer
InfoValue: DBLaTeX
</xsl:text>
  </xsl:template>

  <xsl:template match="*|@*|text()">
    <xsl:apply-templates select="*|@*|text()"/>
  </xsl:template>
</xsl:stylesheet>
